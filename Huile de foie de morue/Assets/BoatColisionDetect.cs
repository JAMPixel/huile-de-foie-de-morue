﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatColisionDetect : MonoBehaviour
{

	public GameObject Oil;
	public GameObject particles;
	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name == "Player")
        {
			Quaternion spawnRotation = Quaternion.identity;
			if(gameObject.name == "Greenpeace(Clone)")
			{
				Score.GetInstance().ScoreOnCollision(ScoreGivers.oilGreenPeace);
				Instantiate(Oil, gameObject.transform.position, spawnRotation);
			}
			else
			{
				Score.GetInstance().ScoreOnCollision(ScoreGivers.solarGreenPeace);
			}
			Instantiate(particles, gameObject.transform.position, spawnRotation);
			this.gameObject.SetActive(false);
            Destroy(this.gameObject);
        }
    }
}
