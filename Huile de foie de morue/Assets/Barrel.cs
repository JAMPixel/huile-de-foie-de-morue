﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrel : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

	// Update is called once per frame
	void Update()
	{
		Speed s = Speed.GetInstance();
		GetComponent<Rigidbody2D>().velocity = new Vector2(0, -1) * s.speed;
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.name == "Player")
		{
			Score.GetInstance().Barrel();
			Score.GetInstance().ScoreOnCollision(ScoreGivers.noScore);
			this.gameObject.SetActive(false);
			Destroy(this.gameObject);
		}
	}
}
