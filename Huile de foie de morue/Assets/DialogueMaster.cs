﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueMaster : MonoBehaviour
{
    public PlayerController pc;
    private bool firstDamage = false;
    private bool greenPeace1 = false;
    private bool greenPeace2 = false;
    private bool oil = false;
    private bool plank = false;

    void Awake()
    {
        StartDialogue("DialogCanvasStart");
    }

    // Update is called once per frame
    void Update()
    {
        if (!firstDamage && pc.pv < pc.MaxHP)
        {
            StartDialogue("DialogCanvasFirstDamage");
            firstDamage = true;
        }
        else if (!plank && Score.instance.score > 30000)
        {
            StartDialogue("DialogCanvasPlanks");
            plank = true;
        }
        else if (!oil && Score.instance.score > 200000)
        {
            StartDialogue("DialogCanvasOil");
            oil = true;
        }
        else if (!greenPeace1 && Score.instance.score > 1000000)
        {
            StartDialogue("DialogCanvasGreenpeace");
            greenPeace1 = true;
        }
        else if (!greenPeace2 && Score.instance.score > 10000000)
        {
            StartDialogue("DialogCanvasGreenpeace2");
            greenPeace2 = true;
        }
    }

    void StartDialogue(String dialogueTag)
    {
        Speed.GetInstance().oldSpeed = Speed.GetInstance().speed;
        Speed.GetInstance().speed = 0;

        transform.Find(dialogueTag).gameObject.SetActive(true);
    }
}
