﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardSpawn : MonoBehaviour
{
	public GameObject hazard;
	public Vector3 spawnValues;
	public float spawnWait;
	public float startWait;
	public float freqMax;
	public float timeToMax;
	void Start()
	{
		StartCoroutine(SpawnWaves());
	}

	IEnumerator SpawnWaves()
	{
		yield return new WaitForSeconds(startWait);
		while (true)
		{
			if (Speed.GetInstance().speed > 1)
			{
				Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
				Quaternion spawnRotation = Quaternion.identity;
				Instantiate(hazard, spawnPosition, spawnRotation);
				yield return new WaitForSeconds(Mathf.Max(spawnWait - (1 / timeToMax) * Time.time, 0f) + Random.Range(0.1f, 0.3f) * freqMax);
			}
			else
			{
				yield return new WaitForSeconds(1);
			}
		}
	}
}