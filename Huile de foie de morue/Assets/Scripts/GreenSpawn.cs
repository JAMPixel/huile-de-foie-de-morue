﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenSpawn : MonoBehaviour
{
	public GameObject green;
	public Vector3 spawnValues;
	public float spawnWait;
	public float startScore;
	
	public float freqMax;
	void Start()
	{
		StartCoroutine(SpawnWaves());
	}

	IEnumerator SpawnWaves()
	{

		while (true)
		{
			if (Score.GetInstance() && Score.GetInstance().score > startScore && Speed.GetInstance().speed > 1)
			{
				Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
				Quaternion spawnRotation = Quaternion.identity;
				Instantiate(green, spawnPosition, spawnRotation);
				yield return new WaitForSeconds(Mathf.Max(spawnWait - 0.2f * Time.time, 0f) + Random.Range(4f, 8f) * freqMax);
			}
			else
			{
				yield return new WaitForSeconds(1);
			}
		}
	}
}

