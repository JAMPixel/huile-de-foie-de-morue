﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Speed : MonoBehaviour
{
	public float speed;
    public float oldSpeed;
	private static Speed instance;
    // Start is called before the first frame update
    void Start()
    {
        
    }

	public static Speed GetInstance()
	{
		if (!instance)
		{
			instance = FindObjectOfType(typeof(Speed)) as Speed;
			if (!instance)
			{
				Debug.Log("No speed instance found");
			}
		}
		return instance;
	}

    // Update is called once per frame
    void Update()
    {
		if (speed > 1)
		{
			speed += 0.000002f * Time.time;
		}
    }
}
