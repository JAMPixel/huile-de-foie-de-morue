﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenMover : MonoBehaviour
{

	// Start is called before the first frame update
	void Start()
	{
		Rigidbody2D rig = GetComponent<Rigidbody2D>();
		if (rig.position.x < 0)
		{
			rig.velocity = new Vector2(Random.Range(0, 3), 0);
			rig.rotation = 180;
		}
		else
		{
			rig.velocity = new Vector2(Random.Range(-3, 0), 0);
		}
	}

	// Update is called once per frame
	void Update()
	{
		Speed s = Speed.GetInstance();
		GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, -1 * s.speed);
	}
}
