﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blackSea : MonoBehaviour
{

	public float angularVelocity;
	public float velocity;
	// Start is called before the first frame update
	void Start()
    {
		//GetComponent<Rigidbody2D>().angularVelocity = angularVelocity;
	}

    // Update is called once per frame
    void Update()
    {
		transform.Rotate(new Vector3(0, 0, angularVelocity), Space.World);
		transform.Translate(new Vector2(0, velocity), Space.World);
        if(transform.position.y > 0)
		{
			Rigidbody2D rigid = GetComponent<Rigidbody2D>();
			velocity = 0;

		}

	}
}
