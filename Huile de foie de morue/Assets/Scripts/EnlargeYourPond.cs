﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnlargeYourPond : MonoBehaviour
{

	public float baseSize;
	public float goalSize;
	public float growingSpeed;

	// Start is called before the first frame update
	void Start()
    {
		transform.localScale = new Vector3(baseSize, baseSize, baseSize);
    }

    // Update is called once per frame
    void Update()
    {
		if (Speed.GetInstance().speed > 1)
		{
			Vector3 goal = new Vector3(goalSize, goalSize, transform.localScale.z);
			transform.localScale = Vector3.Lerp(transform.localScale, goal, Time.deltaTime * growingSpeed);
		}
    }
}
