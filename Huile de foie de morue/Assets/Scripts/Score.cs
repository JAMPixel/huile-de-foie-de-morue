﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ScoreGivers
{
    noScore=0,
    scorePerSecond = 10,
    solarGreenPeace = 500,
    oilGreenPeace = 1000,
    seaShephard = 2000,
    coral = 5000
}

public class Score : MonoBehaviour
{
    public ulong score = 0;

    [SerializeField] private float cooldown = 10;
    [SerializeField] private int multiplier = 1;
    [SerializeField] private PlayerController pc;
    [SerializeField] private Text multiplierText;
    [SerializeField] private Text scoreText;
    [SerializeField] private Image circleCooldown;
    
    public float timeAtLastMultiply = 0;
    private int multiplierMalus = 0;

	public static Score instance;

	public static Score GetInstance()
	{
		return instance;
	}


    // Start is called before the first frame update
    void Start()
    {
		instance = this;
        InvokeRepeating("AddScoreSecond",0, 1);
        timeAtLastMultiply = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	public void Barrel()
	{
		multiplierMalus--;
	}

    public void ScoreOnCollision(ScoreGivers sg)
    {
        addScore((int)sg);
		//TODO : vvvv  Not that  vvvv
		if (pc.MaxHP - pc.pv - multiplierMalus >= 10)
		{
			multiplierMalus = (int)(pc.MaxHP - pc.pv - 10);
		}
        multiplier = (int)Mathf.Pow(2, Mathf.Clamp(pc.MaxHP - pc.pv - multiplierMalus, 0, 10));
        multiplierText.text = "x" + multiplier.ToString();
        timeAtLastMultiply = Time.time;
    }

    private void addScore(int additionalScore)
    {
		Score.GetInstance().score = score + (ulong) additionalScore * (ulong) multiplier;
		Score.GetInstance().scoreText.text = Score.GetInstance().score.ToString();
    }

    void FixedUpdate()
    {
		if (Speed.GetInstance().speed > 1)
		{
			if (multiplier > 1)
			{
				float timeToCooldown = timeAtLastMultiply + cooldown;
				circleCooldown.fillAmount = (timeToCooldown - Time.time) / cooldown;
				if (Time.time > timeToCooldown)
				{
					multiplier /= 2;
					multiplierMalus++;
					multiplierText.text = "x" + multiplier.ToString();
					timeAtLastMultiply = Time.time;
					circleCooldown.fillAmount = 1;
				}
			}
			else
			{
				circleCooldown.fillAmount = cooldown;
			}
		}
    }

    void AddScoreSecond()
    {
		if (Speed.GetInstance().speed > 1)
		{
			addScore((int)ScoreGivers.scorePerSecond);
		}
    }
}
