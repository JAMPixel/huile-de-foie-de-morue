﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class repair : MonoBehaviour
{
	// Start is called before the first frame update
	void Update()
	{
		Speed s = Speed.GetInstance();
		GetComponent<Rigidbody2D>().velocity = new Vector2(0, -1) * s.speed;
	}

	// Update is called once per frame
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.name == "Player")
		{
			PlayerController pc = col.gameObject.GetComponent<PlayerController>();
			pc.pv = (int)Mathf.Min(pc.pv + 1f, pc.MaxHP);
			Score.GetInstance().ScoreOnCollision(ScoreGivers.noScore);
			this.gameObject.SetActive(false);
			Destroy(this.gameObject);
		}
	}
}
