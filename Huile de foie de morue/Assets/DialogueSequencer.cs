﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueSequencer : MonoBehaviour
{
    public GameObject go;
    private Queue<DialogueTrigger> dialogueQueue = new Queue<DialogueTrigger>();

    void Awake()
    {
        DialogueTrigger[] diagTab = GetComponentsInChildren<DialogueTrigger>();
        Debug.Log("TESTING TESTING 123 : DialogueSequencer");
        foreach (var diag in diagTab)
        {
            dialogueQueue.Enqueue(diag);
        }

        launchDialogue();
    }

    public void launchDialogue()
    {
        if (dialogueQueue.Count > 0)
        {
            var diag = dialogueQueue.Dequeue();
            diag.TriggerDialogue();
        }
        else
        {
            dialogueQueue = new Queue<DialogueTrigger>();
            Speed.GetInstance().speed = Speed.GetInstance().oldSpeed;
            go.SetActive(false);
        }
    }
}
