﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coral : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.name == "Player")
		{
			this.gameObject.SetActive(false);
			Destroy(this.gameObject);
			Score.GetInstance().ScoreOnCollision(ScoreGivers.coral);
		}
	}
}
