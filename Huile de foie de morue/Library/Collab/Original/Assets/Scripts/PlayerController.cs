﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private Rigidbody2D rigidbody2D;
    [SerializeField] private Collider2D collider2D;

    [SerializeField] private float speed = 1;
    [SerializeField] public int pv = 10;
    private float maxHP;
    public float MaxHP
    {
        get => maxHP;
    }

    [SerializeField] private float spriteBlinkingTimer = 0.0f;
    [SerializeField] private float spriteBlinkingMiniDuration = 0.1f;

    private float spriteBlinkingTotalTimer = 0.0f;
    private float spriteBlinkingTotalDuration = 1.0f;
    private bool startBlinking = false;

	public GameObject oil;
	public float spawnWait;

    [SerializeField] private Score score;
    [SerializeField] private Camera camera;
    [SerializeField] private Animator animator;

    private float minX;
    private float maxX;

    [SerializeField] private Component gameOverScreen;


    PlayerController()
    {
        maxHP = pv;
    }

    // Start is called before the first frame update
    void Start()
    {
		StartCoroutine(SpawnWaves());
        Vector2 leftCorner = camera.ScreenToWorldPoint(new Vector3(0, 0, 0));
        Vector2 rightCorner = camera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));

        minX = leftCorner.x;
        maxX = rightCorner.x;
    }

	IEnumerator SpawnWaves()
	{
		while (true)
		{
			if (maxHP != pv)
			{
				//for (int i = 0; i < hazardCount; i++)
				{
					Vector3 spawnPosition = transform.position;
					spawnPosition.x += Random.Range(-0.1f,0.1f);
					spawnPosition.y -= 1;
					//spawnPosition.z -= 0.5f;
					Quaternion spawnRotation = Quaternion.identity;
					Instantiate(oil, spawnPosition, spawnRotation);
					//yield return new WaitForSeconds(spawnWait);
				}
			}
			yield return new WaitForSeconds(spawnWait * (pv/2 +1));
		}
	}
	// Update is called once per frame
	void Update()
	{
        if (startBlinking == true)
            SpriteBlinkingEffect();
        if (pv <= maxHP / 2) animator.SetBool("isBoatRuined",true);
	    if (pv <= 0)
	    {
	        Debug.Log("La planete est sauvée NOOOOOOOOOOOOOOOOOOOOOOOOOOO !!!!");
	    }

        var v3 = transform.position;
        v3.x = Mathf.Clamp(v3.x, minX, maxX);
        transform.position = v3;
    }

    void FixedUpdate()
    {
        rigidbody2D.velocity = new Vector2(Input.GetAxis("Horizontal")*speed, 0);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
		if (col.gameObject.name != "Boundary" && col.gameObject.tag != "Decoration")
		{

			Debug.Log(col.gameObject.name + " : " + gameObject.name + " : " + Time.time);
			if (!startBlinking)
			{
				pv--;
				if (pv == maxHP - 1) this.transform.GetChild(0).gameObject.SetActive(true);
			}
			startBlinking = true;
            score.ScoreOnCollision(ScoreGivers.noScore);
		}
    }

    private void SpriteBlinkingEffect()
    {
        spriteBlinkingTotalTimer += Time.deltaTime;
        if (spriteBlinkingTotalTimer >= spriteBlinkingTotalDuration)
        {
            startBlinking = false;
            spriteBlinkingTotalTimer = 0.0f;
            foreach (var spriteRenderer in this.gameObject.GetComponentsInChildren<SpriteRenderer>())
            {
                spriteRenderer.enabled = true;
            }
            return;
        }

        spriteBlinkingTimer += Time.deltaTime;
        if (spriteBlinkingTimer >= spriteBlinkingMiniDuration)
        {
            spriteBlinkingTimer = 0.0f;
            if (this.gameObject.GetComponent<SpriteRenderer>().enabled == true)
            {
                foreach (var spriteRenderer in this.gameObject.GetComponentsInChildren<SpriteRenderer>())
                {
                    spriteRenderer.enabled = false;
                }
            }
            else
            {
                foreach (var spriteRenderer in this.gameObject.GetComponentsInChildren<SpriteRenderer>())
                {
                    spriteRenderer.enabled = true;
                }
            }
        }
    }
}
